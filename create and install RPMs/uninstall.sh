#!/bin/sh

sudo rpm -e <project_name>
sudo rm -R build dist mygit/__pycache__
sudo rm -r <project_name>.egg-info/
