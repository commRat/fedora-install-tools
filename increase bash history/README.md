Bash history size on fedora is set to 1000 lines as default, which is not enough if you use bash a lot as I do. Your history can become a powerful tool if you forgot often as I do :)

For example if you don't remember the exact command for docker usage, you can use this:

    history | grep docker

Which list all your commands where you used "docker".

Here is how to increase your bash history size. Copy this lines:

    HISTFILESIZE=90000
    HISTSIZE=90000

And insert them to /.bashrc. You can open it with nano by this command:

    nano ~/.bashrc

It does not has to be only 90K lines, you can set it to whatever you feel comfortable with. You can also have infinite history if you leave it empty like this:

    HISTFILESIZE=
    HISTSIZE=
